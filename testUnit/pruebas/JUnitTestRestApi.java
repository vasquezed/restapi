/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import WebService.service.EmployeeFacadeREST;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author usuario
 */
public class JUnitTestRestApi {
    
    public JUnitTestRestApi() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void TestJunitRestCount() {
    
        EmployeeFacadeREST instance = new EmployeeFacadeREST();
        String count=instance.countREST();
        assertNotNull(count);   
    }
    
    @Test
    public void TestJunitRestRemove() {
    
        EmployeeFacadeREST instance = new EmployeeFacadeREST();
        instance.remove(1);  
    }
    
    @Test
    public void TestJunitRestCreate() {
    
        EmployeeFacadeREST instance = new EmployeeFacadeREST();
        instance.create(entity);  
    }
    
    @Test
    public void TestJunitRestEdit() {
    
        EmployeeFacadeREST instance = new EmployeeFacadeREST();
        instance.edit(1);  
    }
    
    @Test
    public void TestJunitFind() {
    
        EmployeeFacadeREST instance = new EmployeeFacadeREST();
        instance.find(1);  
    }
}
