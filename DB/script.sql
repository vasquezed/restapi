--------------- Primer consulta a ejecutar -----------------------------
-- Database: "Prueba_BancoBogota"
CREATE DATABASE "Prueba_BancoBogota"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish_Colombia.1252'
       LC_CTYPE = 'Spanish_Colombia.1252'
       CONNECTION LIMIT = -1;
-------------------------------------------------------------------------

---------------------- Segunda consulta a ejecutar ----------------------
-- Table: public.employee
CREATE TABLE public.employee
(
  id_employee integer NOT NULL,
  nombre character varying(50),
  funcion character varying(100),
  id_boss integer,
  CONSTRAINT "Employee_pkey" PRIMARY KEY (id_employee)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.employee
  OWNER TO postgres;
-------------------------------------------------------------------------