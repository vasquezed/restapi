## Uso y compilación del proyecto pruebaRestApi:

** Fecha de Creación: ** 07 de Agosto de 2020

------

** Descripción del proyecto **

El proyecto pruebaRestApi esta construido en Java web application y contiene los metodos POST,GET,PUT y DELETE de la tabla empleados 

** Requisitos para el proyecto **

1. IDE NetBeans 8.1 o 8.2.
2. Server GlassFish server 4.1.1.
3. Contar con un JDK de java instalado.
4. Instalar Postman como cliente para consumir los metodos (Deseable)
5. Instalar Postgresql para base de datos.
6. Acceso al repositorio del proyecto RestApi (https://bitbucket.org/vasquezed/restapi/src/master/).

** Nota: ** El segundo ítem por lo general están incluidos con la instalación del IDE NetBeans, pero se hace necesario una vez instalado el IDE revisar que cuente con dichos requisitos.

* ### Cargue o creacion de la base de datos:


      1. #### Crear Base de datos con scrit SQL del respositorio:
      
       
        * En el entorno de PostgreSql (pgAdmin) abrir el script SQL y ejecutar las consultas expuestas. 
		
      2. #### Crear base de datos cargando backup:
      
       
        * En el entorno de PostgreSql (pgAdmin) abrir el script SQL y ejecutar solo el primer comando. 
		* Selecionar la DB creada y dar clic derecho. 
        * Selecionar la opción "Restore". 
        * En la ventana desplegada oprimir el boton "...".
        * Selecionar la ruta del backup de la DB, este backup debe ser descargado del repositorio.
        * Una vez seleccionado backup de la DB se oprime el boton "Restore".
        * Una vez finalizado el cargue de la DB se oprime el boton Done. 

* ### Cargar proyecto al IDE :


      1. #### Crear conexion con la DB:
      
       
        * En el entorno de NetBeans selecionar la pestaña service en la parte superior izquierda. 
        * En el menu desplegado selecionar database.
        * Dar clic derecho sobre database y seleccionar nueva conexion.
        * Selecionar del menu desplegable "Driver" la opción PostgreSql y oprimir el boton "siguente".
        * en la ventana desplegada colocar el nombre de la base de datos anteriormente creada y colocar la contraseña se se asigno cuando se instalo el server de PostgreSql.
        * Una vez modificado o agregado esos campos oprima el boton "probar conexion".
        * Este debe arrojar exitoso si no verifique los parametro colocados.		
		
      2. #### Cargar proyecto al IDE:
      
       
        * Una vez creada la conexion con la DB, se debe cargar el proyecto al IDE NetBeans para esto se debe descargar del repositorio la carpeta "pruebaRestApi". 
		* Despues se debe seleccionar file en la parte superio del IDE. 
        * Seleccionar open project. 
        * Selecionar la carpeta descargada del repositorio y oprimir el boton "Open project".
        * Una vez cargado el proyecto se puede dar compilar.

* ### Consumir WS por medio de Postman :


      1. #### Se recomienda utilizar Postman como cliente para consumir y probar los WS.
	  2. #### URLS de consumo WS:
      
       
        * Servicio GET listado de datos tabla Employee:http://localhost:8080/pruebaRestApi/webresources/webservice.employee  
        * Servicio GET listado de datos tabla Employee por ID:http://localhost:8080/pruebaRestApi/webresources/webservice.employee/{id}
        * Servicio POST para insert en tabla Employee:http://localhost:8080/pruebaRestApi/webresources/webservice.employee 
        * Servicio PUT para modificar registro de la tabla Employee por ID:http://localhost:8080/pruebaRestApi/webresources/webservice.employee/{id}
		* Servicio DELETE para eliminar registro de la tabla Employee por ID:http://localhost:8080/pruebaRestApi/webresources/webservice.employee/{id}
		* Servicio GET para visualizar la catidad de registros de la tabla Employee:http://localhost:8080/pruebaRestApi/webresources/webservice.employee/count
	
	
** Nota: ** Para los metodos POST y PUT se debe enviar un el body un XML con los datos a insertar o modificar como el siguiente.

	<employee>
        <funcion>supervisor</funcion>
        <idBoss>0</idBoss>
        <idEmployee>2</idEmployee>
        <nombre>Ramiro</nombre>
    </employee>
	
** Nota: ** Para las urls que contienen "{id}" esto se debe remplazar con el id a consultar,modificar o borrar.
		
  